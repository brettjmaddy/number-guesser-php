<?php
    require_once('Connection.php');
    session_start();
        
    $select = "SELECT `User`, `Score` FROM `epiz_21388289_cs3750`.`Scores` WHERE `Score` > 0 ORDER BY `Score` ASC LIMIT 10";
    
    $success = $con-> query($select);
    
    if($success == FALSE)
    {
        $failmess = "Whole query " . $select . "<br>";
        echo $failmess;
        die('Invalid Query: ' . mysqli_error($con));
    }
    
    echo "<center><h2>High Scores</h2></center><br><br>"
        . "<center><table border = 1>";
    
    while($row = $success->fetch_assoc())
    {        
            echo "<tr><td>".$row['User']
            ."</td><td>"   .$row['Score']
            ."</td></tr>";            
    }
    
    echo "</table></center>";
?>

<html>
    <head>
    </head>
    <body>
        <center><form action="Game.php" method="POST">
                <br><br>
                    <input type ="submit" value="Back">
            </form></center>
    </body>    
</html>


