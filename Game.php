<?php
    session_start();
?>

<html>
    <head> 
        <script
            src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous">
        </script>
        
        <script>
            $(document).ready(function(){
               var form = $('#GameForm');
               
               form.submit(function(event){
                  var guess = $('#guess').val();
                  
                  if ( $.trim(guess) !== '')
                  {
                      $.post('GameResult.php', {number: guess}, function(data){
                         
                         if(guess > 100 || guess < 1)
                         {
                             alert("Please choose a number between 1 and 100"); 
                         }
                         else
                         {
                            alert(data);
                         }
                      });
                  }
                  
                  event.preventDefault();
               });
            });
        </script>
    </head>
        <body> 
        <center><h2>Pick a Number Between 1 and 100</h2></center><br>
        <center><form action="GameResult.php" method="POST" id="GameForm">
                Enter Your Guess:<br><br>
                <input type="text" name ="guess" id="guess" required><br><br>
                <input type="submit" id="guess" value="Guess"><br>           
            </form></center>
                        
            <center><form action="DisplayScores.php" method="POST">
                <input type="submit" id="DisplayScores" value="Display Scores">
                </form></center>
            <center><form action="LoginPage.php">
                <input type="submit" id="Logout" value="Logout">
                </form></center>           
        </body>    
</html>
