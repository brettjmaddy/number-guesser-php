<?php
    require_once('Connection.php');
    session_start(); 
    
    $guess = htmlentities($_POST['number']);
    
    $user = $_SESSION['User'];
    
    $score;
    
    $highOrLow = "";        
        
    if($guess < $_SESSION['Number'])
    {
        $highOrLow = "Too low";
        $_SESSION['Score'] = $_SESSION['Score'] + 1;
    }
    else if($guess > $_SESSION['Number'])
    {
        $highOrLow = "Too high";
        $_SESSION['Score'] = $_SESSION['Score'] + 1;
    }
    else
    {
        $highOrLow = "That's right!";
        
        $_SESSION['Score'] = $_SESSION['Score'] + 1;        
        $score = $_SESSION['Score'];
                
        $insertScore = "INSERT INTO `epiz_21388289_cs3750`.`Scores`(`User`, `Score`)"
            . "VALUES('$user', $score)";      
        
        $successScore = $con->query($insertScore);
    
        if($successScore == FALSE)
        {
            $fail = "Whole query " . $insertScore . "<br>";
            echo $fail;
            die('Invalid Query: ' . mysqli_error($con));
        }
        
        $_SESSION['Number'] = mt_rand(1, 100);
    }
    
    echo $highOrLow;    
?>    
    