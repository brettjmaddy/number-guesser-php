<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
    require_once "Connection.php";
    session_unset();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Number Game</title>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/core.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/hmac.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/sha256.js"></script>
        
        <script>
            $(document).ready(function(){                
               var form = $('#loginform');
               form.submit(function(event){
                    var user = $('#Username').val();
                    var password = $('#Password').val();
                    var hash = CryptoJS.SHA256(password);
                    var result = CryptoJS.enc.Hex.stringify(hash);
                    $.post('LoginCheck.php', {Username: user, Password: result}, function(data){
                        
                        alert(data);
                       if(data.includes("Bad"))
                       {
                           alert("Incorrect Username or Password");
                       }
                       else
                       {
                           window.location ="Game.php";
                       }                      
                    });
                    
                  event.preventDefault();
               });
            });
        </script>       
    </head>
    
    <body>
        <center><h2>Login Page</h2></center>
        <center><form id="loginform" action ="LoginCheck.php" method = "POST">
                Username:<br>
                <input type="text" id="Username" name="Username" required><br><br>
                Password:<br>
                <input type="password" id="Password" name="Password" required><br><br>
                <input type="submit" value="Login" name="Login"><br>
                </form></center>

        <center><form action="Register.php" method ="POST">
                -or-<br>
                <input type="submit" value="Register" name="Register"><br>
                </form></center>        
    </body>
</html>
