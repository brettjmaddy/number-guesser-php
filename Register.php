<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<html>
    <head>
        <meta charset="UTF-8">
        <title>Register</title> 
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/core.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/hmac.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/sha256.js"></script>
        
        <script>
            $(document).ready(function(){                
               var form = $('#registerform');
               form.submit(function(event){
                    var name = $('#Name').val();
                    var user = $('#Username').val();
                    var password = $('#Password').val();
                    var hash = CryptoJS.SHA256(password);
                    var result = CryptoJS.enc.Hex.stringify(hash);
                    
                    $.post('ConfirmRegister.php', {Name: name, Username: user, Password: result}, function(data){
                        
                       if(data.includes("Bad"))
                       {
                           alert("Username already exists");
                       }
                       else
                       {
                           window.location ="Game.php";
                       }                      
                    });
                  
                  event.preventDefault();
               });
            });
        </script>
    </head>
    
    
    <body>
        <center><h2>Please Enter Your Information</h2></center>

        <center><form id="registerform" action="ConfirmRegister.php" method="POST" class="ajax">
                    Name:<br>
                    <input type="text" name="Name" id="Name" required><br><br>
                    Username:<br>
                    <input type="text" name="Username" id="Username" required><br><br>
                    Password:<br>
                    <input type="password" name="Password" id="Password" required><br><br>
                    <input type="submit" value="Submit">
                </form></center>
            <br>
            <br>
        <center><form action ="LoginPage.php">
                    <input type="submit" value ="Return">
                </form><center>            
    </body>
</html>

